#include "theTrie.h"

#define convertToIndex(c) ((int)c - (int)'a')

node *novoNo(void) {
    node *pNode = NULL;
    pNode = (node *) malloc(sizeof(node));
    if (pNode) {
        int i;
        pNode->isFinalWord = false;
        for (i = 0; i < 26; i++) //Tamanho do alfabeto
            pNode->children[i] = NULL;
    }
    return pNode;
}


void inserirTrie(string palavra, node *trie) { //insere na trie ou marca o ultimo como isFinalWord
    int altura;
    int tamanho_palavra = strlen(palavra);
    int index;

    node *no_trie = trie;

    for (altura = 0; altura < tamanho_palavra; altura++) {
        index = convertToIndex(palavra[altura]);
        if (!no_trie->children[index])
            no_trie->children[index] = novoNo();

        no_trie = no_trie->children[index];
    }


    no_trie->isFinalWord = true; //marca ultima folha da trie
    no_trie->count++;

}

// Retorna true se presente na trie
bool pesquisaComContagem(string palavra, node *trie, int *repeat) {
    int altura;
    int tamanho_palavra;
    tamanho_palavra = strlen(palavra);
    int index;
    node *no_trie = trie;
    for (altura = 0; altura < tamanho_palavra; altura++) {
        index = convertToIndex(palavra[altura]);
        if (!no_trie->children[index])
            return false;
        no_trie = no_trie->children[index];
    }
    *repeat = no_trie->count;
    return (no_trie->isFinalWord);
}


int contadorPalavras(node *trie_busca, char *termo) {
    if (trie_busca == NULL)
        return 0;
    node *atual = trie_busca;

    while (*termo) {
        atual = atual->children[*termo - 'a']; //proximo elemento
        if (atual == NULL) // Final da trie
            return 0;
        termo++;
    }
    return atual->count;
}


void geraTrieArquivo(string nome_arquivo, node *trie_busca, int n, PalavrasFiltradas *tops) {
    FILE *arquivo = fopen(nome_arquivo, "r");
    char buffer[512];
    printf("Processando %s. Aguarde...\n", nome_arquivo);
    while (fscanf(arquivo, "%s", buffer) != EOF) {
        palavraParaMinusculo(buffer);
        inserirTrie(buffer, trie_busca);
        int count = contadorPalavras(trie_busca, buffer);

        for (int i = 0; i < n; i++) {
            if (count > tops[i].count) {
                tops[i].count = count;
                strcpy(tops[i].word, buffer);
                break;
            }
        }
    }
    fclose(arquivo);

}


int contadorPalavrasTotal(node *trie_busca) {
    int result = 0;

    if (trie_busca->isFinalWord) //caso final da palavra
        result += trie_busca->count;

    for (int i = 0; i < 26; i++)
        if (trie_busca->children[i])
            result += contadorPalavrasTotal(trie_busca->children[i]);
    return result;
}




