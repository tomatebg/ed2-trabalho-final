#include <stdio.h>
#include <string.h>
#include "theTrie.h"
#include "utils.h"
#include "opcoes.h"

int main(int argc, string argv[]) {
    if (argc <= 3) {
        printf("Trabalho Final ED2 - Indexador \n");
        printf("Alunos: Mario Neto e Rogério Bayer \n");
        printf("Opções:\n");
        printf("--freq N ARQUIVO \nExibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.\n\n");
        printf("--freq-word PALAVRA ARQUIVO \nExibe o número de ocorrências de PALAVRA em ARQUIVO. \n\n");
        printf("--search TERMO ARQUIVO [ARQUIVO ...]\nExibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO. A listagem é apresentada em ordem descrescente de relevância. TERMO pode conter mais de uma palavra. Neste caso, deve ser indicado entre àspas.\n");
    } else if (strcmp(argv[1], "--freq") == 0) {
        opcaoFreq(argv);
    } else if (strcmp(argv[1], "--freq-word") == 0) {
        opcaoFreqWord(argv[3], argv[2]);
    } else if (strcmp(argv[1], "--search") == 0) {
        opcaoSearch(argc, argv);
    } else {
        printf("Os parâmetros apresentados estão errados. Verifique a opção desejada e tente novamente\n");
    }
}

 

